package kuznikos.restapp.restapp.tmp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestContrik{

    @Autowired
    UserRepository repository;

    @GetMapping("/all")
    public Iterable<User> getAll() {
        return repository.findAll();
    }

    @GetMapping("/create")
    public ResponseEntity<User> create(){
        return ResponseEntity.ok(repository.save(new User(21L, "Kostya")));
    }
}
